const express = require('express');
const router = express.Router();
const Huesped = require('../models/huesped');
const Incidencias = require('../models/incidencias');
const User = require('../models/User');




router.get('/welcome', async (req, res) => {
    res.render('welcome');
});


router.get('/', async (req, res) => {
  const lel = await Incidencias.find();
  console.log(lel);
  res.render('index', {
    lel
  });
});

router.post('/', async (req, res, next) => {
  const incidencias = new Incidencias(req.body);
  await incidencias.save();
  res.redirect('/');
});

router.post('/huesped', async (req, res, next) => {
    const huesped = new Huesped(req.body);
    await huesped.save();
    res.redirect('/');
  });

module.exports = router;