const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const huespedSchema = new Schema ({

nombre_huesped: String,
apellidos_huesped: String,
no_huesped: String,
tipo_huesped: Number,
numero_condominio: Number

});

module.exports = mongoose.model('huesped', huespedSchema);