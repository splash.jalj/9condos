const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const huesped = mongoose.model('huesped');

const incidenciasSchema = new Schema({
   
fecha_reporte: { type: Date, default: Date.now },
problema_detectado: String,
area_problema: String,
comentario_reporte: String,
huesped: { type: Schema.ObjectId, ref: "huesped"}
});

module.exports = mongoose.model('incidencias', incidenciasSchema);