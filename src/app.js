const path = require('path');
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
const app = express();
require('./config/passport');

//importando rutas
const indexRoutes = require('./routes/index');
app.use('/static', express.static('./public'));//Aquí se almacenan los datos estáticos

//Conexión a la base de datos
mongoose.set('useFindAndModify', false);
mongoose.connect('mongodb://localhost/9condos', {
    useNewUrlParser: true,
    useCreateIndex: true
}).then(db => console.log('Base de datos conectada'))
    .catch(err => console.log(error));
console.log(Date.now);

//Configuraciones
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views')); //indica la ruta de nuestra carpeta de vistas
app.set('view engine', 'ejs');
app.listen(app.get('port'), () => console.log('Servidor en el puerto',app.get('port')));

//Middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(methodOverride('_method'));

app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));

app.use(session({ secret: 'anything' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//Variables globales
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
  });

//Rutas
app.use('/', indexRoutes);//le quité el punto a la raiz
app.use(require('./routes/users'));